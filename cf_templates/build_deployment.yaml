---
AWSTemplateFormatVersion: 2010-09-09

Description: CFN Template to deploy CodePipeline to deploy Online Boutique Application to EKS following DevSecOps Practices

Parameters:
  CodeCommitRepositoryName:
    Type: String
    Description: Name of AWS CodeCommit repo to be created
  CodeCommitRepositoryBranchName:
    Type: String
    Default: main
    Description: Default Branch
  CodeCommitRepositoryS3Bucket:
    Type: String
    Default: sre-aws-online-boutique
    Description: Name of S3 Bucket where zip file of initial code will be stored
  CodeCommitRepositoryS3BucketObjKey:
    Type: String
    Default: code/cicdstack.zip
    Description: S3 ObjectKey, where intial code zip file is stored inside S3 bucket 
  EKSClusterName:
    Type: String
    Description: EKS Cluster Name
  EcrDockerRepository:
    Type: String
    Default: aws-online-boutique-repository
    Description: ECR Docker Repositry Name
  EmailRecipient:
    Type: String
    Default: user@domain.com
    Description: Email Address for Build Notifications
  MySecurityHub:
    Type: String
    Default: MySecurityHub
    Description: For Accepting Security Recommendations

Resources:

  MyRepo:
    Type: AWS::CodeCommit::Repository
    Properties:
      RepositoryName: !Ref CodeCommitRepositoryName
      RepositoryDescription: This is a repository for my project with code from CodeCommitRepositoryS3Bucket.
      Code:
        BranchName: !Ref CodeCommitRepositoryBranchName
        S3: 
          Bucket: !Ref CodeCommitRepositoryS3Bucket
          Key: !Ref CodeCommitRepositoryS3BucketObjKey

  SourceRepositoryAssociation:
    Type: AWS::CodeGuruReviewer::RepositoryAssociation
    Properties:
      Name: !Ref CodeCommitRepositoryName
      Type: CodeCommit
      BucketName: !Ref CodeGuruReviewerBucket

  BuildPipelineSNSTopic:
    Type: AWS::SNS::Topic
    Properties:
      KmsMasterKeyId: alias/aws/sns
      TopicName:
        Fn::Sub: PipelineSNSTopic-${AWS::StackName}

  BuildPipelineSNSSubscription:
    Type: AWS::SNS::Subscription
    Properties:
      Endpoint: !Ref EmailRecipient
      Protocol: email
      TopicArn: !Ref 'BuildPipelineSNSTopic'

  BuildPipelineSnsPolicy:
    Type: AWS::SNS::TopicPolicy
    Properties:
      Topics: [ !Ref BuildPipelineSNSTopic ]
      PolicyDocument: !Sub |
        {
          "Version": "2012-10-17",
          "Id": "__default_policy_ID",
          "Statement": [
            {
              "Sid": "__default_statement_ID",
              "Effect": "Allow",
              "Principal": {
                "AWS": "*"
              },
              "Action": [
                "SNS:GetTopicAttributes",
                "SNS:SetTopicAttributes",
                "SNS:AddPermission",
                "SNS:RemovePermission",
                "SNS:DeleteTopic",
                "SNS:Subscribe",
                "SNS:ListSubscriptionsByTopic",
                "SNS:Publish",
                "SNS:Receive"
              ],
              "Resource": "${BuildPipelineSNSTopic}",
              "Condition": {
                "StringEquals": {
                  "AWS:SourceOwner": "${AWS::AccountId}"
                }
              }
            },
            {
              "Sid": "Allow Events",
              "Effect": "Allow",
              "Principal": {
                "Service": "events.amazonaws.com"
              },
              "Action": "sns:Publish",
              "Resource": "${BuildPipelineSNSTopic}"
            }
          ]
        }

  CodeBuildImageStateEventRule:
    Type: "AWS::Events::Rule"
    Properties:
      Description: "Rule for sending code buildimage project notifications to SNS topic"
      EventPattern:
        source:
          - "aws.codebuild"
        detail-type:
          - "CodeBuild Build State Change"
        detail:
          project-name:
            - !Sub 'Build-${AWS::StackName}'
          build-status:
            - "FAILED"
            - "SUCCEEDED"
            - "IN_PROGRESS"
            - "STOPPED"
      State: "ENABLED"
      Targets:
        - Arn: !Ref BuildPipelineSNSTopic
          Id: CodeBuildNotificationTest
          InputTransformer:
            InputPathsMap:
              build-id: "$.detail.build-id"
              project-name: "$.detail.project-name"
              build-status: "$.detail.build-status"
              account: "$.account"
              region: "$.region"
              time: "$.time"
              resourcesarn: "$.resources"
              deep-link: "$.detail.additional-information.logs.deep-link"
            InputTemplate: |
              {
                 "buildStatus": "Build has reached status of <build-status> at <time>",
                 "buildID" : <build-id>,
                 "buildProject": <project-name>,
                 "buildProjectArn": <resourcesarn>,
                 "awsAccount": <account>,
                 "awsRegion": <region>,
                 "buildLogs": <deep-link>
              }
    DependsOn: [ "CodeBuildImageProject" ]

  CodeDeployToDevImageStateEventRule:
    Type: "AWS::Events::Rule"
    Properties:
      Description: "Rule for sending code deploy to dev image project notifications to SNS topic"
      EventPattern:
        source:
          - "aws.codebuild"
        detail-type:
          - "CodeBuild Build State Change"
        detail:
          project-name:
            - !Sub 'Deploy-${AWS::StackName}'
          build-status:
            - "FAILED"
            - "SUCCEEDED"
            - "IN_PROGRESS"
            - "STOPPED"
      State: "ENABLED"
      Targets:
        - Arn: !Ref BuildPipelineSNSTopic
          Id: CodeBuildNotificationTest
          InputTransformer:
            InputPathsMap:
              build-id: "$.detail.build-id"
              project-name: "$.detail.project-name"
              build-status: "$.detail.build-status"
              account: "$.account"
              region: "$.region"
              time: "$.time"
              resourcesarn: "$.resources"
              deep-link: "$.detail.additional-information.logs.deep-link"
            InputTemplate: |
              {
                 "buildStatus": "Build has reached status of <build-status> at <time>",
                 "buildID" : <build-id>,
                 "buildProject": <project-name>,
                 "buildProjectArn": <resourcesarn>,
                 "awsAccount": <account>,
                 "awsRegion": <region>,
                 "buildLogs": <deep-link>
              }
    DependsOn: [ "CodeDeployToDevImageProject" ]

  CodeDeployToTestImageStateEventRule:
    Type: "AWS::Events::Rule"
    Properties:
      Description: "Rule for sending code deploy to test image project notifications to SNS topic"
      EventPattern:
        source:
          - "aws.codebuild"
        detail-type:
          - "CodeBuild Build State Change"
        detail:
          project-name:
            - !Sub 'Deploy-${AWS::StackName}'
          build-status:
            - "FAILED"
            - "SUCCEEDED"
            - "IN_PROGRESS"
            - "STOPPED"
      State: "ENABLED"
      Targets:
        - Arn: !Ref BuildPipelineSNSTopic
          Id: CodeBuildNotificationTest
          InputTransformer:
            InputPathsMap:
              build-id: "$.detail.build-id"
              project-name: "$.detail.project-name"
              build-status: "$.detail.build-status"
              account: "$.account"
              region: "$.region"
              time: "$.time"
              resourcesarn: "$.resources"
              deep-link: "$.detail.additional-information.logs.deep-link"
            InputTemplate: |
              {
                 "buildStatus": "Build has reached status of <build-status> at <time>",
                 "buildID" : <build-id>,
                 "buildProject": <project-name>,
                 "buildProjectArn": <resourcesarn>,
                 "awsAccount": <account>,
                 "awsRegion": <region>,
                 "buildLogs": <deep-link>
              }
    DependsOn: [ "CodeDeployToTestImageProject" ]

  CodeDeployToProdImageStateEventRule:
    Type: "AWS::Events::Rule"
    Properties:
      Description: "Rule for sending code deploy to Prod image project notifications to SNS topic"
      EventPattern:
        source:
          - "aws.codebuild"
        detail-type:
          - "CodeBuild Build State Change"
        detail:
          project-name:
            - !Sub 'Deploy-${AWS::StackName}'
          build-status:
            - "FAILED"
            - "SUCCEEDED"
            - "IN_PROGRESS"
            - "STOPPED"
      State: "ENABLED"
      Targets:
        - Arn: !Ref BuildPipelineSNSTopic
          Id: CodeBuildNotificationTest
          InputTransformer:
            InputPathsMap:
              build-id: "$.detail.build-id"
              project-name: "$.detail.project-name"
              build-status: "$.detail.build-status"
              account: "$.account"
              region: "$.region"
              time: "$.time"
              resourcesarn: "$.resources"
              deep-link: "$.detail.additional-information.logs.deep-link"
            InputTemplate: |
              {
                 "buildStatus": "Build has reached status of <build-status> at <time>",
                 "buildID" : <build-id>,
                 "buildProject": <project-name>,
                 "buildProjectArn": <resourcesarn>,
                 "awsAccount": <account>,
                 "awsRegion": <region>,
                 "buildLogs": <deep-link>
              }
    DependsOn: [ "CodeDeployToProdImageProject" ]

  CodePipelineStateEventRule:
    Type: "AWS::Events::Rule"
    Properties:
      Description: "Rule for sending codepipeline notifications to SNS topic"
      EventPattern:
        source:
          - "aws.codepipeline"
        detail-type:
          - "CodePipeline Pipeline Execution State Change"
        detail:
          pipeline:
            - !Sub 'Pipeline-${AWS::StackName}'
          state:
            - "CANCELED"
            - "SUCCEEDED"
            - "STARTED"
            - "STOPPED"
            - "RESUMED"
            - "STOPPING"
            - "SUPERSEDED"
      State: "ENABLED"
      Targets:
        - Arn: !Ref BuildPipelineSNSTopic
          Id: CodePipelineNotificationTest
          InputTransformer:
            InputPathsMap:
              pipeline: "$.detail.pipeline"
              state: "$.detail.state"
              account: "$.account"
              region: "$.region"
              time: "$.time"
              resourcesarn: "$.resources"
            InputTemplate: |
              {
                 "pipelineStatus": "Pipeline <pipeline> has reached state of <state> at <time>",
                 "pipelineArn": <resourcesarn>,
                 "awsAccount": <account>,
                 "awsRegion": <region>
              }
    DependsOn: [ "CodePipelineCodeCommit" ]

  CodePipelineArtifactBucket:
    Type: AWS::S3::Bucket
    Properties:
      PublicAccessBlockConfiguration:
        IgnorePublicAcls: true
        BlockPublicAcls: true
        BlockPublicPolicy: true
        RestrictPublicBuckets: true
      VersioningConfiguration:
        Status: Enabled
      LoggingConfiguration:
        DestinationBucketName: !Ref LoggingBucket
        LogFilePrefix: codeartifact-logs
      BucketEncryption:
        ServerSideEncryptionConfiguration: 
        - ServerSideEncryptionByDefault:
            SSEAlgorithm: AES256
    DeletionPolicy: Retain
    UpdateReplacePolicy: Retain

  CodeGuruReviewerBucket:
    Type: AWS::S3::Bucket
    Properties:
      BucketName: !Sub codeguru-reviewer-${AWS::Region}-${AWS::StackName}
      PublicAccessBlockConfiguration:
        IgnorePublicAcls: true
        BlockPublicAcls: true
        BlockPublicPolicy: true
        RestrictPublicBuckets: true
      VersioningConfiguration:
        Status: Enabled
      LoggingConfiguration:
        DestinationBucketName: !Ref LoggingBucket
        LogFilePrefix: codegurureviewer-logs
      BucketEncryption:
        ServerSideEncryptionConfiguration: 
        - ServerSideEncryptionByDefault:
            SSEAlgorithm: AES256
    DeletionPolicy: Retain
    UpdateReplacePolicy: Retain

  LoggingBucket:
    Type: AWS::S3::Bucket
    Properties:
      PublicAccessBlockConfiguration:
        IgnorePublicAcls: true
        BlockPublicAcls: true
        BlockPublicPolicy: true
        RestrictPublicBuckets: true
      OwnershipControls:
        Rules:
          - ObjectOwnership: ObjectWriter
      VersioningConfiguration:
        Status: Enabled
      AccessControl: LogDeliveryWrite
      BucketEncryption:
        ServerSideEncryptionConfiguration:
          - ServerSideEncryptionByDefault:
              SSEAlgorithm: AES256
    DeletionPolicy: Retain
    UpdateReplacePolicy: Retain

  CodePipeineBucketPolicy:
    Type: AWS::S3::BucketPolicy
    Properties:
      Bucket: !Ref CodePipelineArtifactBucket
      PolicyDocument:
        Statement:
          - Action:
              - s3:PutObject
              - s3:GetObject
              - s3:GetObjectVersion
              - s3:GetBucketVersioning
            Effect: Allow
            Resource:
              - !Sub arn:${AWS::Partition}:s3:::${CodePipelineArtifactBucket}
              - !Sub arn:${AWS::Partition}:s3:::${CodePipelineArtifactBucket}/*
            Principal:
              AWS:
                - !GetAtt [CodePipelineServiceRole,Arn]
                - !GetAtt [CodeBuildServiceRole,Arn]
                - !GetAtt [EksCodeBuildkubeRole,Arn]

  CodeReviewerBucketPolicy:
    Type: AWS::S3::BucketPolicy
    Properties:
      Bucket: !Ref CodeGuruReviewerBucket
      PolicyDocument:
        Statement:
          - Action:
              - s3:PutObject
              - s3:GetObject
              - s3:GetObjectVersion
              - s3:GetBucketVersioning
            Effect: Allow
            Resource:
              - !Sub arn:${AWS::Partition}:s3:::${CodeGuruReviewerBucket}
              - !Sub arn:${AWS::Partition}:s3:::${CodeGuruReviewerBucket}/*
            Principal:
              AWS:
                - !GetAtt [CodePipelineServiceRole,Arn]
                - !GetAtt [CodeBuildServiceRole,Arn]
                - !GetAtt [EksCodeBuildkubeRole,Arn]

  LogBucketPolicy:
    Type: AWS::S3::BucketPolicy
    Properties:
      Bucket: !Ref LoggingBucket
      PolicyDocument:
        Statement:
          - Action:
              - s3:PutObject
            Effect: Allow
            Resource:
              - !Sub arn:${AWS::Partition}:s3:::${LoggingBucket}
              - !Sub arn:${AWS::Partition}:s3:::${LoggingBucket}/codeartifact-logs/*
              - !Sub arn:${AWS::Partition}:s3:::${LoggingBucket}/codegurureviewer-logs/*
            Principal:
              Service: logging.s3.amazonaws.com
            Condition:
              ArnLike:
                'aws:SourceArn':
                  - !Sub arn:${AWS::Partition}:s3:::${CodePipelineArtifactBucket}
                  - !Sub arn:${AWS::Partition}:s3:::${CodeGuruReviewerBucket}
              StringEquals:
                'aws:SourceAccount':
                  - !Sub ${AWS::AccountId}

  CodePipelineServiceRole:
    Type: AWS::IAM::Role
    Properties:
    #  RoleName:
    #    Fn::Sub: CodePipelineServiceRole-${AWS::StackName}
      Path: /service-role/
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Principal:
              Service: codepipeline.amazonaws.com
            Action: sts:AssumeRole
      Policies:
        - PolicyName: codepipeline-access
          PolicyDocument:
            Version: 2012-10-17
            Statement:
              - Resource: !Sub arn:${AWS::Partition}:codecommit:${AWS::Region}:${AWS::AccountId}:${CodeCommitRepositoryName}
                Effect: Allow
                Action:
                  - codecommit:GetBranch
                  - codecommit:GetCommit
                  - codecommit:ListRepositories
                  - codecommit:GetRepository
                  - codecommit:UploadArchive
                  - codecommit:GetUploadArchiveStatus
                  - codecommit:CancelUploadArchive
              - Resource:
                  - !Sub arn:${AWS::Partition}:codebuild:${AWS::Region}:${AWS::AccountId}:project/${CodeBuildImageProject}
                  - !Sub arn:${AWS::Partition}:codebuild:${AWS::Region}:${AWS::AccountId}:project/${CodeDeployToDevImageProject}
                  - !Sub arn:${AWS::Partition}:codebuild:${AWS::Region}:${AWS::AccountId}:project/${CodeDeployToTestImageProject}
                  - !Sub arn:${AWS::Partition}:codebuild:${AWS::Region}:${AWS::AccountId}:project/${CodeDeployToProdImageProject}
                  - !Sub arn:${AWS::Partition}:codebuild:${AWS::Region}:${AWS::AccountId}:project/${CodeSecScanProject}
                Effect: Allow
                Action:
                  - codebuild:StartBuild
                  - codebuild:BatchGetBuilds
                  - codebuild:BatchGetBuildBatches
                  - codebuild:StartBuildBatch
              - Resource: !GetAtt SourceRepositoryAssociation.AssociationArn
                Effect: Allow
                Action:
                  - codeguru-reviewer:List*
                  - codeguru-reviewer:Describe*
                  - codeguru-reviewer:Get*
                  - codeguru:Get*
                  - codeguru-reviewer:CreateCodeReview
              - Resource: !Sub arn:${AWS::Partition}:ecr:${AWS::Region}:${AWS::AccountId}:repository/${EcrDockerRepository}
                Effect: Allow
                Action:
                  - ecr:DescribeImages
              - Resource: !Sub arn:${AWS::Partition}:securityhub:${AWS::Region}:*
                Effect: Allow
                Action:
                  - securityhub:Get*
                  - securityhub:List*
                  - securityhub:Describe*
                  - config:DescribeConfigurationRecorders
                  - config:DescribeConfigurationRecorderStatus
                  - config:DescribeConfigRules
                  - config:BatchGetResourceConfig
                  - config:SelectResourceConfig
                  - config:PutEvaluations
                  - logs:DescribeMetricFilters
                  - cloudtrail:DescribeTrails
                  - cloudtrail:GetTrailStatus
                  - cloudtrail:GetEventSelectors
                  - cloudwatch:DescribeAlarms
                  - cloudwatch:DescribeAlarmsForMetric
                  - iam:GenerateCredentialReport
                  - iam:GetCredentialReport
              - Resource: !Sub arn:${AWS::Partition}:securityhub:${AWS::Region}::product/aquasecurity/aquasecurity
                Effect: Allow
                Action:
                  - securityhub:BatchImportFindings
              - Resource: !Sub arn:${AWS::Partition}:s3:::${CodePipelineArtifactBucket}/*
                Effect: Allow
                Action:
                  - s3:PutObject
                  - s3:GetObject
                  - s3:GetObjectVersion
                  - s3:GetBucketVersioning
    DependsOn: CodePipelineArtifactBucket

  CodeBuildServiceRole:
    Type: AWS::IAM::Role
    Properties:
    #  RoleName:
    #    Fn::Sub: CodeBuildServiceRole-${AWS::StackName}
      Path: /service-role/
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Principal:
              Service:
                - codebuild.amazonaws.com
                - codepipeline.amazonaws.com
            Action: sts:AssumeRole
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryPowerUser
      Policies:
        - PolicyName: codebuild-access-1
          PolicyDocument:
            Version: 2012-10-17
            Statement:
              - Resource: !Sub arn:${AWS::Partition}:securityhub:${AWS::Region}:*
                Effect: Allow
                Action:
                  - securityhub:Get*
                  - securityhub:List*
                  - securityhub:Describe*
                  - securityhub:BatchImportFindings
                  - config:DescribeConfigurationRecorders
                  - config:DescribeConfigurationRecorderStatus
                  - config:DescribeConfigRules
                  - config:BatchGetResourceConfig
                  - config:SelectResourceConfig
                  - config:PutEvaluations
                  - logs:DescribeMetricFilters
                  - cloudtrail:DescribeTrails
                  - cloudtrail:GetTrailStatus
                  - cloudtrail:GetEventSelectors
                  - cloudwatch:DescribeAlarms
                  - cloudwatch:DescribeAlarmsForMetric
                  - iam:GenerateCredentialReport
                  - iam:GetCredentialReport
              - Resource: !Sub arn:${AWS::Partition}:securityhub:${AWS::Region}::product/aquasecurity/aquasecurity
                Effect: Allow
                Action:
                  - securityhub:BatchImportFindings
        - PolicyName: codebuild-access-2
          PolicyDocument:
            Version: 2012-10-17
            Statement:
              - Resource: arn:aws:logs:*:*:*
                Effect: Allow
                Action:
                  - logs:CreateLogGroup
                  - logs:CreateLogStream
                  - logs:PutLogEvents
              - Resource: !Sub arn:${AWS::Partition}:codecommit:${AWS::Region}:${AWS::AccountId}:${CodeCommitRepositoryName}
                Effect: Allow
                Action:
                  - codecommit:GitPull
                  - codecommit:TagResource
              - Resource: "*"
                Effect: Allow
                Action:
                  - codeguru-reviewer:AssociateRepository
              - Resource: 
                  - !GetAtt SourceRepositoryAssociation.AssociationArn
                  - !Sub arn:${AWS::Partition}:codeguru-reviewer:${AWS::Region}:${AWS::AccountId}:*
                Effect: Allow
                Action:
                  - codeguru-reviewer:List*
                  - codeguru-reviewer:Describe*
                  - codeguru-reviewer:Get*
                  - codeguru:Get*
                  - codeguru-reviewer:CreateCodeReview
              - Resource: !Sub arn:${AWS::Partition}:eks:${AWS::Region}:${AWS::AccountId}:cluster/${EKSClusterName}
                Effect: Allow
                Action:
                  - eks:DescribeNodegroup
                  - eks:DescribeUpdate
                  - eks:DescribeCluster
              - Resource: 
                  - !Sub arn:${AWS::Partition}:s3:::${CodePipelineArtifactBucket}
                  - !Sub arn:${AWS::Partition}:s3:::${CodeGuruReviewerBucket}
                Effect: Allow
                Action:
                  - s3:ListBucket
              - Resource: 
                  - !Sub arn:${AWS::Partition}:s3:::${CodePipelineArtifactBucket}/*
                  - !Sub arn:${AWS::Partition}:s3:::${CodeGuruReviewerBucket}/*
                Effect: Allow
                Action:
                  - s3:GetObject
                  - s3:GetObjectAcl
                  - s3:PutObject
                  - s3:GetObjectVersion
                  - s3:GetBucketAcl
                  - s3:GetBucketLocation
              - Resource: !Sub arn:${AWS::Partition}:ecr:${AWS::Region}:${AWS::AccountId}:repository/${EcrDockerRepository}
                Effect: Allow
                Action:
                  - ecr:GetDownloadUrlForLayer
                  - ecr:BatchGetImage
                  - ecr:BatchCheckLayerAvailability
                  - ecr:PutImage
                  - ecr:InitiateLayerUpload
                  - ecr:UploadLayerPart
                  - ecr:CompleteLayerUpload
                  - ecr:GetAuthorizationToken

  EksCodeBuildkubeRole:
    Type: AWS::IAM::Role
    Properties:
    #  RoleName:
    #    Fn::Sub: EksCodeBuildkubeRole-${AWS::StackName}
      Path: /
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Principal:
              AWS: !Sub arn:${AWS::Partition}:iam::${AWS::AccountId}:role/service-role/${CodeBuildServiceRole}
            Action: sts:AssumeRole
      Policies:
        - PolicyName: codebuild-eks-access
          PolicyDocument:
            Version: 2012-10-17
            Statement:
              - Resource: !Sub arn:${AWS::Partition}:eks:${AWS::Region}:${AWS::AccountId}:cluster/${EKSClusterName}
                Effect: Allow
                Action:
                  - eks:DescribeNodegroup
                  - eks:DescribeUpdate
                  - eks:DescribeCluster

  CodeSecScanProject:
    Type: AWS::CodeBuild::Project
    Properties:
      Artifacts:
        Type: CODEPIPELINE
      Source:
        Location:
          Fn::Sub: https://git-codecommit.${AWS::Region}.amazonaws.com/v1/repos/${CodeCommitRepositoryName}
        Type: CODEPIPELINE
        BuildSpec: "buildspec/buildspec_secscan.yaml"
      TimeoutInMinutes: 30
      EncryptionKey: alias/aws/s3
      Environment:
        ComputeType: BUILD_GENERAL1_SMALL
        Type: LINUX_CONTAINER
        Image: "aws/codebuild/standard:5.0"
        PrivilegedMode: True
        EnvironmentVariables:
          - Name: AWS_DEFAULT_REGION
            Value: !Ref AWS::Region
          - Name: AWS_ACCOUNT_ID
            Value: !Ref AWS::AccountId
          - Name: CG_REVIEWER_BUCKET_NAME
            Value: !Ref CodeGuruReviewerBucket
          - Name: CODE_REVIEW_NAME
            Value: code-review-${AWS::StackName}
          - Name: REPOSITORY_ARN
            Value: !GetAtt CodeBuildServiceRole.Arn
      Name:
        Fn::Sub: SecScan-${AWS::StackName}
      ServiceRole:
        Fn::GetAtt: [ CodeBuildServiceRole, Arn ]

  CodeBuildImageProject:
    Type: AWS::CodeBuild::Project
    Properties:
      Artifacts:
        Type: CODEPIPELINE
      Source:
        Location:
          Fn::Sub: https://git-codecommit.${AWS::Region}.amazonaws.com/v1/repos/${CodeCommitRepositoryName}
        Type: CODEPIPELINE
        BuildSpec: "buildspec/buildspec.yml"
      TimeoutInMinutes: 30
      EncryptionKey: alias/aws/s3
      Environment:
        ComputeType: BUILD_GENERAL1_SMALL
        Type: LINUX_CONTAINER
        Image: "aws/codebuild/standard:5.0"
        PrivilegedMode: True
        EnvironmentVariables:
          - Name: AWS_DEFAULT_REGION
            Value: !Ref AWS::Region
          - Name: IMAGE_REPO_NAME
            Value: !Ref EcrDockerRepository
          - Name: AWS_ACCOUNT_ID
            Value: !Ref AWS::AccountId
      Name:
        Fn::Sub: Build-${AWS::StackName}
      ServiceRole:
        Fn::GetAtt: [ CodeBuildServiceRole, Arn ]

  CodeDeployToDevImageProject:
    Type: AWS::CodeBuild::Project
    Properties:
      Artifacts:
        Type: CODEPIPELINE
      Source:
        Location:
          Fn::Sub: https://git-codecommit.${AWS::Region}.amazonaws.com/v1/repos/${CodeCommitRepositoryName}
        Type: CODEPIPELINE
        BuildSpec: "buildspec/buildspec_dev_deploy.yml"
      TimeoutInMinutes: 30
      EncryptionKey: alias/aws/s3
      Environment:
        ComputeType: BUILD_GENERAL1_SMALL
        Type: LINUX_CONTAINER
        Image: "aws/codebuild/standard:5.0"
        PrivilegedMode: True
        EnvironmentVariables:
          - Name: AWS_DEFAULT_REGION
            Value: !Ref AWS::Region
          - Name: IMAGE_REPO_NAME
            Value: !Ref EcrDockerRepository
          - Name: AWS_ACCOUNT_ID
            Value: !Ref AWS::AccountId
          - Name: EKS_CLUSTER_NAME
            Value: !Ref EKSClusterName
          - Name: EKS_CODEBUILD_ROLE_ARN
            Value: !GetAtt EksCodeBuildkubeRole.Arn
      Name:
        Fn::Sub: Dev-Deploy-${AWS::StackName}
      ServiceRole:
        Fn::GetAtt: [ CodeBuildServiceRole, Arn ]

  CodeDeployToTestImageProject:
    Type: AWS::CodeBuild::Project
    Properties:
      Artifacts:
        Type: CODEPIPELINE
      Source:
        Location:
          Fn::Sub: https://git-codecommit.${AWS::Region}.amazonaws.com/v1/repos/${CodeCommitRepositoryName}
        Type: CODEPIPELINE
        BuildSpec: "buildspec/buildspec_test_deploy.yml"
      TimeoutInMinutes: 30
      EncryptionKey: alias/aws/s3
      Environment:
        ComputeType: BUILD_GENERAL1_SMALL
        Type: LINUX_CONTAINER
        Image: "aws/codebuild/standard:5.0"
        PrivilegedMode: True
        EnvironmentVariables:
          - Name: AWS_DEFAULT_REGION
            Value: !Ref AWS::Region
          - Name: IMAGE_REPO_NAME
            Value: !Ref EcrDockerRepository
          - Name: AWS_ACCOUNT_ID
            Value: !Ref AWS::AccountId
          - Name: EKS_CLUSTER_NAME
            Value: !Ref EKSClusterName
          - Name: EKS_CODEBUILD_ROLE_ARN
            Value: !GetAtt EksCodeBuildkubeRole.Arn
      Name:
        Fn::Sub: Test-Deploy-${AWS::StackName}
      ServiceRole:
        Fn::GetAtt: [ CodeBuildServiceRole, Arn ]

  CodeDeployToProdImageProject:
    Type: AWS::CodeBuild::Project
    Properties:
      Artifacts:
        Type: CODEPIPELINE
      Source:
        Location:
          Fn::Sub: https://git-codecommit.${AWS::Region}.amazonaws.com/v1/repos/${CodeCommitRepositoryName}
        Type: CODEPIPELINE
        BuildSpec: "buildspec/buildspec_prod_deploy.yml"
      TimeoutInMinutes: 30
      EncryptionKey: alias/aws/s3
      Environment:
        ComputeType: BUILD_GENERAL1_SMALL
        Type: LINUX_CONTAINER
        Image: "aws/codebuild/standard:5.0"
        PrivilegedMode: True
        EnvironmentVariables:
          - Name: AWS_DEFAULT_REGION
            Value: !Ref AWS::Region
          - Name: IMAGE_REPO_NAME
            Value: !Ref EcrDockerRepository
          - Name: AWS_ACCOUNT_ID
            Value: !Ref AWS::AccountId
          - Name: EKS_CLUSTER_NAME
            Value: !Ref EKSClusterName
          - Name: EKS_CODEBUILD_ROLE_ARN
            Value: !GetAtt EksCodeBuildkubeRole.Arn
      Name:
        Fn::Sub: Prod-Deploy-${AWS::StackName}
      ServiceRole:
        Fn::GetAtt: [ CodeBuildServiceRole, Arn ]

  CodePipelineCodeCommit:
    Type: AWS::CodePipeline::Pipeline
    Properties:
      Name:
        Fn::Sub: Pipeline-${AWS::StackName}
      RoleArn:
          Fn::GetAtt: [ CodePipelineServiceRole, Arn ]
      ArtifactStore:
        Type: S3
        Location: !Sub ${CodePipelineArtifactBucket}
      Stages:
        - Name: Source
          Actions:
            - Name: App
              ActionTypeId:
                Category: Source
                Owner: AWS
                Version: 1
                Provider: CodeCommit
              Configuration:
                BranchName: !Ref CodeCommitRepositoryBranchName
                OutputArtifactFormat: CODEBUILD_CLONE_REF
                RepositoryName:
                  Ref: CodeCommitRepositoryName
              OutputArtifacts:
                - Name: SourceCodeApp
              RunOrder: 1
        - Name: CodeSecurityScan
          Actions:
            - Name: SecScan
              ActionTypeId:
                Category: Build
                Owner: AWS
                Version: 1
                Provider: CodeBuild
              Configuration:
                ProjectName: !Ref CodeSecScanProject
              InputArtifacts:
                - Name: SourceCodeApp
              OutputArtifacts:
                - Name: SecScanOutput
              RunOrder: 2
        - Name: Build
          Actions:
            - Name: Build
              ActionTypeId:
                Category: Build
                Owner: AWS
                Version: 1
                Provider: CodeBuild
              Configuration:
                ProjectName: !Ref CodeBuildImageProject
              InputArtifacts:
                - Name: SourceCodeApp
              OutputArtifacts:
                - Name: BuildOutput
              RunOrder: 3
        - Name: DeployToDev
          Actions:
            - Name: DeployToDev
              ActionTypeId:
                Category: Build
                Owner: AWS
                Version: 1
                Provider: CodeBuild
              Configuration:
                ProjectName: !Ref CodeDeployToDevImageProject
              InputArtifacts:
                - Name: SourceCodeApp
              OutputArtifacts:
                - Name: DeployDevOutput
              RunOrder: 4
        - Name: DeployToTest
          Actions:
            - Name: DeployToTest
              ActionTypeId:
                Category: Build
                Owner: AWS
                Version: 1
                Provider: CodeBuild
              Configuration:
                ProjectName: !Ref CodeDeployToTestImageProject
              InputArtifacts:
                - Name: SourceCodeApp
              OutputArtifacts:
                - Name: DeployTestOutput
              RunOrder: 5
        - Name: ApprovalToDeploy
          Actions:
            - Name: Approval
              ActionTypeId:
                Category: Approval
                Owner: AWS
                Version: 1
                Provider: Manual
              Configuration:
                CustomData: Approval or Reject the build to be deployed on EKS
              InputArtifacts: []
              OutputArtifacts: []
              RunOrder: 6
        - Name: DeployToProd
          Actions:
            - Name: DeployToProd
              ActionTypeId:
                Category: Build
                Owner: AWS
                Version: 1
                Provider: CodeBuild
              Configuration:
                ProjectName: !Ref CodeDeployToProdImageProject
              InputArtifacts:
                - Name: SourceCodeApp
              OutputArtifacts:
                - Name: DeployOutput
              RunOrder: 7
    DependsOn: [ "BuildPipelineSNSTopic", "BuildPipelineSNSSubscription" ]

Outputs:
  HubArn:
    Description: ARN of AWS Securityhub enabled
    Value: !Ref MySecurityHub
  SNSTopic:
    Description: SNS TopicName
    Value: !Ref BuildPipelineSNSTopic
  CodeBuildServiceRoleARN:
    Description: ARN of CodeBuildservicerole
    Value: !GetAtt CodeBuildServiceRole.Arn
  EksCodeBuildkubeRoleARN:
    Description: ARN of EksCodeBuildkubeRole which will be added in aws-auth configmap for deployment to EKS
    Value: !GetAtt EksCodeBuildkubeRole.Arn
  CodePipelineURL:
    Description: Codepipeline URL for this stack
    Value:
      Fn::Join:
        - ""
        - - "https://console.aws.amazon.com/codepipeline/home?region="
          - Ref: AWS::Region
          - "#/view/"
          - Ref: CodePipelineCodeCommit
  CodeCommitRepoUrl:
    Value:
      Fn::Join:
        - ""
        - - "https://console.aws.amazon.com/codecommit/repositories/home?region="
          - Ref: AWS::Region
          - "#/view/"
          - Ref: MyRepo
